import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullcalendarComponent } from './fullcalendar/fullcalendar.component';
import { PrimengcalendarComponent } from './primengcalendar/primengcalendar.component';
import { SwitchComponent } from './switch/switch.component';
import { FieldsComponent } from './fields/fields.component';
import { AppComponent } from './app.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { MyChartComponent } from './my-chart/my-chart.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { MaterialDemoComponent } from './material-demo/material-demo.component';


const routes: Routes = [
  {
    path: 'primengcalendar',
    component: PrimengcalendarComponent
  },
  {
    path: 'fullcalendar',
    component: FullcalendarComponent
  },
  {
    path: 'switch',
    component: SwitchComponent
  },
  {
    path: 'fields',   
    component: FieldsComponent
  },
  {
    path: 'app',
    component: AppComponent
  },
  {
    path: 'line-chart',
    component: LineChartComponent
  },
  {
    path: 'my-chart',
    component: MyChartComponent
  },
  {
    path: 'pie-chart',
    component: PieChartComponent
  },
  {
    path: 'material-demo',
    component: MaterialDemoComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }



export const routingComponents = [FullcalendarComponent, PrimengcalendarComponent, SwitchComponent, LineChartComponent, MyChartComponent, PieChartComponent, MaterialDemoComponent]
