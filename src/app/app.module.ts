import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';
import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import { CalendarModule } from 'primeng/calendar'; // primeng calendar
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { SwitchComponent } from './switch/switch.component';
import { FieldsComponent } from './fields/fields.component';
import { FullcalendarComponent } from './fullcalendar/fullcalendar.component';
import { PrimengcalendarComponent } from './primengcalendar/primengcalendar.component';
import { RouterModule } from '@angular/router';
import { MyChartComponent } from './my-chart/my-chart.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { MatButtonModule } from '@angular/material/button';
import { MaterialDemoComponent } from './material-demo/material-demo.component';
import { MatCardModule } from '@angular/material/card';


FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin
]);

// const appRoutes: Routes = [
//   {
//     path: 'primengcalendar', 
//     component: PrimengcalendarComponent
//   },
//   {
//     path: 'fullcalendar',
//     component: FullcalednarComponent
//   },
  // {
  //   path: '',
  //   redirectTo: '/hello',
  //   pathMatch: 'full'
  // }, // redirect to `first-component`
  
// ];

@NgModule({
  declarations: [
    AppComponent,
    FullcalendarComponent,
    PrimengcalendarComponent,
    routingComponents,
    SwitchComponent,
    FieldsComponent,
    MyChartComponent,
    PieChartComponent,
    LineChartComponent,
    MaterialDemoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FullCalendarModule, // register FullCalendar with you app
    CalendarModule,      // primeng calendar
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule
    // RouterModule.forRoot(appRoutes, { enableTracing: false, useHash: true }),
  ],

  exports: [RouterModule],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


