import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimengcalendarComponent } from './primengcalendar.component';

describe('PrimengcalendarComponent', () => {
  let component: PrimengcalendarComponent;
  let fixture: ComponentFixture<PrimengcalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimengcalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimengcalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
